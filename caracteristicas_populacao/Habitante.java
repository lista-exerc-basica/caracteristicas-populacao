

public class Habitante
{
    private int idade;
    private String sexo, corOlhos, corCabelo;
    
    public Habitante() {   
    }
    
    public Habitante(String sexo, int idade, String corOlhos, String corCabelo) {
        this.sexo = sexo;
        this.idade = idade;
        this.corOlhos = corOlhos;
        this.corCabelo = corCabelo;

    }
    
    public boolean ehDoPerfil() {
        return (this.sexo.equalsIgnoreCase("F")) && (this.idade >= 18 && this.idade <= 35) && (this.corOlhos.equalsIgnoreCase("V")) 
        && (this.corCabelo.equalsIgnoreCase("L"));
    }
    
    public void setSexo(String sexo) {
        if(sexo.equalsIgnoreCase("M") || sexo.equalsIgnoreCase("F")) {
            this.sexo = sexo;    
        }
    }
    
    public String getSexo() {
        return this.sexo;   
    }
    
    public void setIdade(int idade) {
        this.idade = idade;   
    }
    
    public int getIdade() {
        return this.idade;   
    }
    
    public void setCorOlhos(String corOlhos) {
        this.corOlhos = corOlhos;   
    }
    
    public String getCorOlhos() {
        return this.corOlhos;   
    }
    
    public void setCorCabelo(String corCabelo) {   
        this.corCabelo = corCabelo;                    
    }
    
    public String getCorCabelo() {
        return this.corCabelo;   
    }
}
