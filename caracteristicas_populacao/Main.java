import java.util.Scanner;


public class Main
{
    public static void main(String[] args) {
        
        Scanner le = new Scanner(System.in);
        Pesquisa pesq = new Pesquisa();
        
        
        System.out.println("Inicio da pesquisa");
        while(true) {
            Habitante h = new Habitante();
            System.out.println("Informe o sexo (M) / (f)");
            h.setSexo(le.next());
            
            System.out.println("Informe a cor dos olhos (A)zul, (V)erde, (C)astanho");
            h.setCorOlhos(le.next());
            
            System.out.println("Informe a cor dos cabelos (L)oiro, (C)astanho, (P)reto");
            h.setCorCabelo(le.next());           
            
            System.out.println("Informe a idade");
            h.setIdade(le.nextInt());
            
            pesq.addHabitante(h);
            
            System.out.println("Gostaria de incluir mais um habitante na pesquisa? (S)im/(N)ão");
            if(le.next().equalsIgnoreCase("N")) {
                break;
            }
            
        }
    
        System.out.println(pesq.totalDoPerfil());
        
        System.out.println("##RESULTADO PESQUISA##");
        
        System.out.println("O habitante mais velho: " + pesq.habitanteMaisVelho());
        System.out.println();
        System.out.print("O habitante mais novo: " + pesq.habitanteMaisNovo());
        System.out.println();
        System.out.println("A quantidade de pessoas do sexo masculino são: " + pesq.totalDeHomens()  
        + " que equivale a " +pesq.porcentagemDeHomens() + "% da população");
        System.out.println();
        System.out.println("O percentual de habitantes do sexo feminino é equivalente a: " + pesq.porcentagemDeMulheres() + "%");
        System.out.println();
        System.out.println("O percentual de habitantes do sexo feminino equivalente ao perfil proposto é de: " + pesq.porcentagemDoPerfil() + "%");
        System.out.println();
        System.out.print("A média das idades das mulheres é: " + pesq.mediaIdadeMulheres());
        
    }
}
