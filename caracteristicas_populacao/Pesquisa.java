import java.util.ArrayList;

public class Pesquisa
{
    private ArrayList<Habitante> habitantes;

    public Pesquisa() {
        this.habitantes = new ArrayList<>();
    }
    
    public int totalDeHomens() {
        int totalHomens = 0;
        
        for(Habitante h : habitantes) {
            if(h.getSexo().equalsIgnoreCase("M")) {
                totalHomens++;   
            }
        }
        
        return totalHomens;
    }
    
    public int totalIdaDeMulheres() {
        int totalIdadeMulheres = 0;
        
        for(Habitante h : habitantes) {
            if(h.getSexo().equalsIgnoreCase("F")) {
                totalIdadeMulheres += h.getIdade();    
            }
        }
        
        return totalIdadeMulheres;
    }
    
    public int totalDeMulheres() {
        int totalMulheres =0;
        
        for(Habitante h : habitantes) {
            if(h.getSexo().equalsIgnoreCase("F")) {
                totalMulheres++;    
            }
        }
        
        return totalMulheres;
    }
    
    public int totalDoPerfil() {
        int totalPerfil = 0;
        
        for(Habitante h : habitantes) {
            if(h.ehDoPerfil()) {
              totalPerfil++;     
            }
        }
        
        return totalPerfil;
    }
    
    public int totalHabitantes() {
        return totalDeMulheres() + totalDeHomens();
    }
    
    public double mediaIdadeMulheres() {
        return  totalIdaDeMulheres() / (totalDeMulheres() + 0.0);  
    }
    
    public double porcentagemDeHomens() {
        return totalDeHomens() / (totalHabitantes() / 100.0);
    }
    
    public double porcentagemDeMulheres() {
        return totalDeMulheres() / (totalHabitantes() / 100.0);
    }
    
    public double porcentagemDoPerfil() {
        return totalDoPerfil() / (totalHabitantes() / 100.0);  
    }
    
    public int habitanteMaisVelho() {
        int maior = 0;
        
        for(Habitante h : habitantes) {
            if(h.getIdade() >= maior) {
                maior = h.getIdade();   
            }
        }
        
        return maior;
    }
    
    public int habitanteMaisNovo() {
        int menor = habitanteMaisVelho();
        
        for(Habitante h : habitantes) {
            if(h.getIdade() <= menor) {
                menor = h.getIdade();   
            }
        }
        
        return menor;
    }
    
    public void addHabitante(Habitante habitantes) {
        this.habitantes.add(habitantes);   
    }
    
    public void removeHabitantes(Habitante habitantes) {
        this.habitantes.remove(habitantes);   
    }
  
    public ArrayList<Habitante> getHabitantes() {
        return this.habitantes;   
    }
}


